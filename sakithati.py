import sys,os,importlib
from time import time,sleep
from past.builtins import execfile
import lib.mvc as mvc
import lib.util_log as util_log


log = util_log.logger()

if (len(sys.argv) == 1):
    log.error("Please include parameter")
    sys.exit(0)

### DYNAMICALLY IMPORT SETTING DOT PY
filename_settings="apps.%s.settings"%(sys.argv[1])
try:
    PARAMETER = importlib.import_module(filename_settings)
except ImportError:
    log.error("Settings file not found: %s"%(filename_settings))
    log.error("Exiting ....")
    sys.exit()

log.info('Current directory: %s'%(os.getcwd()))
package_folder='apps%s%s%spackage%s'%(os.sep,sys.argv[1],os.sep,os.sep)
log.info('Inserting package file location into python env var: %s'%package_folder)
os.chdir(package_folder)
sys.path.insert(0, os.getcwd())
os.chdir('../..')

### LOAD INCLUDE FILE
loadmvcfile = 'apps.'+sys.argv[1]+'.logic'
#LOGIC_MVC = importlib.import_module(loadmvcfile)
try:
    LOGIC_MVC = importlib.import_module(loadmvcfile)
except ImportError:
    log.exception("problem while loading LOGIC")
    sys.exit()

try:
    class_name="mvc_%s"%(sys.argv[1])
    klass = getattr(LOGIC_MVC,class_name)
except AttributeError:
    log.exception('problem while loading %s',class_name)
    exit()
if issubclass(klass,mvc.mvc_base) is False:
    log.error('Class: %s is not inherit class mvc_base',class_name)
    exit()

dynamic_keyword_argument={
    'log':log,
    'counter1':1,
    'PARAMETER':PARAMETER,
    'source':sys.argv[1]
}

xxx=klass(**dynamic_keyword_argument)
xxx.startup()
while 1:
    xxx.run()
    if xxx.break_the_loop()==True:
        break

log.error("Program Logic Ended")
log.error("Exiting")


########################## END CORE ENGINE ##########################
