
class generic_class():

    def __init__(self,**kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)

    def inject(self,**kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)
