# -*- coding: utf-8 -*-

from random import randint
from datetime import datetime
import re

__author__ = 'Ishafizan'


# for play/test brand translation
def format_brand_play(log, mytext):
    log.info("brnd play %s"%(mytext))
    if mytext.upper() == "NAJIB_NEGATIVE":
        etxt = "NAJIB_NEGATIVE"
    elif mytext.upper() == "NAJIB_POSITIVE":
        etxt = "NAJIB_POSITIVE"
    elif mytext.upper() == "NEUTRAL":
        etxt = "NEUTRAL"
    else:
        etxt = "0"
    return etxt


# 2016-01-09 split list into number of parts
def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts]
            for i in range(wanted_parts)]


# write to file
def write_to_file(fo_train, content, perception):
    fo_train.write("%s\t%s" % (content, perception) + "\n")
    # added this flush data if parallel
    fo_train.flush()


# remove multiple spaces
def remove_multiple_whitespace(mytext):
    mytext = " ".join(mytext)
    return mytext


word_re = re.compile("\w+")


# def get word count
def word_count(message):
    # word_count
    try:
        my_word_count = len(re.findall(r'\b\w+\b', message))
        # words = message.get_text().split()
    except Exception, ex:
        print "\t", ex
        my_word_count = 0
    return my_word_count


# randomise sleep
def get_sleep_time(one, two):
    timer = randint(one, two)
    return timer


# get current time
def getcurrdt():
    dt = datetime.now()
    dt = dt.strftime("%Y-%m-%d %H:%M:%S")
    return dt


# get current dt epoch
def getcurrdtepoch():
    dt = datetime.now()
    # dt = dt.strftime("%Y-%m-%d %H:%M:%S")
    dt = int(dt.strftime("%s")) * 1000
    return dt
