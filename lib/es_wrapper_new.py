from json import dumps
from time import sleep
from elasticsearch import Elasticsearch, exceptions as es_exceptions
import pprint


# based class
class es_server():
    def __init__(self,es_source,es_source_index,log):
        self.es_source_index=es_source_index
        self.es_source=es_source
        self.es_source_client = Elasticsearch(es_source)
        self.log=log

    def getting(self,query_source,options=dict(scroll="1m", size=3, timeout=10, request_timeout=5)):
        scroll_id=0
        while True:
            try:
                resc = self.es_source_client.search(index=self.es_source_index,body=query_source,search_type="scan",**options)
            except es_exceptions.ConnectionError:
                self.log.info("Connection Time Out. Reconnect in 5 minutes")
                sleep(300)
                continue
            else:
                scroll_Id = resc['_scroll_id']
                while True:
                    scroll_id=scroll_id+1
                    try:
                        self.log.info('Scroll %s'%(scroll_id))
                        esres = self.es_source_client.scroll(scroll_id=scroll_Id, scroll="5m")
                    except es_exceptions.TransportError:
                        break
                    else:
                        if not esres['hits']['hits']:
                            self.log.info("finish")
                            break
                        else:
                            res_posts = esres["hits"]["hits"]
                            yield res_posts
            break

    def sending(self,bulk_list,timeout):
        bulk_body = '\n'.join(map(dumps, bulk_list)) + '\n'
        self.log.info('BULK BODY: %s'%(bulk_body))
        output=self.es_source_client.bulk(body=bulk_body, request_timeout=timeout)
        self.log.info("-----------------------------------")
        self.log.info(output)
        self.log.info("===================================")


#outgoing ES extend es_server class
class es_spoolOut(es_server):
    def __init__(self,es_source,es_source_index,log,limitbulk=2):
        es_server.__init__(self,es_source,es_source_index,log)
        self.pp = pprint.PrettyPrinter(indent=4)
        self.spool=[]
        self.limitbulk=limitbulk

    def append(self,fbid,var):
        self.spool.append({"update": {"_id": fbid, "_index": "%s" % self.es_source_index, "_type": "posts" }})
        self.spool.append(var)

    def send(self):
        total=len(self.spool)/2
        x=1
        y=1
        bulk_list=[]
        while True:
            try:
                bulk_list.append(self.spool.pop(0))
                bulk_list.append(self.spool.pop(0))
            except IndexError:
                self.log.info("SENDING FINAL REMAINING IN QUEUE")
                if len(bulk_list) > 0:
                    self.sending(bulk_list,240)
                    self.log.info("SENT: %s, TOTAL SENT: %s, TOTAL IN QUEUE: %s"%(x,y,total))
                else:
                    self.log.info("SENT: %s, TOTAL SENT: %s, TOTAL IN QUEUE: %s"%(x,y,total))
                break
            else:
                self.log.info("PROCESSED: %s"%(x))
                if x >= self.limitbulk:
                    self.sending(bulk_list,240)
                    self.log.info("FINALISING")
                    self.log.info("SENT: %s, TOTAL SENT: %s, TOTAL IN QUEUE: %s"%(x,y,total))
                    x=1
                    bulk_list=[]
                else:
                    x=x+1
            y=y+1

#incoming ES extend es_server class
class es_spoolIn(es_server):
    def __init__(self,es_source,es_source_index,log):
        self.pp = pprint.PrettyPrinter(indent=4)
        es_server.__init__(self,es_source,es_source_index,log)
        self.es_source_index=es_source_index

    def get(self,query_source):
        get_num=0
        for res_posts in self.getting(query_source):
            page_count=0
            get_num=get_num+1
            for res_post in res_posts:
                page_count=page_count+1
                self.log.info('Page Count: %s'%(page_count))
                yield(res_post,get_num,page_count)
