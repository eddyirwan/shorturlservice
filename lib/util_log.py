# -*- coding: utf-8 -*-
import logging
import os.path
import sys

boto_log = logging.getLogger("boto")
boto_log.setLevel(logging.CRITICAL)
es_log = logging.getLogger("elasticsearch")
es_log.setLevel(logging.CRITICAL)
urllib3_log = logging.getLogger("urllib3")
urllib3_log.setLevel(logging.CRITICAL)

# init logger
def logger():
    program = os.path.basename(sys.argv[0])
    log = logging.getLogger(program)
    #logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s [%(filename)s:%(lineno)d]')
    logging.basicConfig(format='%(threadName)-10s %(asctime)s : %(levelname)s : %(message)s')
    logging.root.setLevel(level=logging.INFO)
    log.info("running %s" % ' '.join(sys.argv))
    return log
