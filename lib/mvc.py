import abc

class mvc_base ():
    __metaclass__ = abc.ABCMeta

    def __init__(self,**kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)
        self.break_yeild=False

    def inject(self,**kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)

    def break_the_loop(self):
        return self.break_yeild

    @abc.abstractmethod
    def run(self):
        """run"""

    @abc.abstractmethod
    def startup(self):
        """start"""
