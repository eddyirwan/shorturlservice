# -*- coding: utf-8 -*-
import util_gen
from json import dumps
import re
import string
import regex
import codecs

__author__ = 'Ishafizan'
# mac /usr/local/lib/python2.7/site-packages/stop_words/stop-words
# linux /usr/local/lib/python2.7/dist-packages/stop_words-2015.2.23.1-py2.7.egg/stop_words/stop-words

# extras
word_list = ["respectmypm", "membangunbangsa", "memakmurnegeri", "kekalnajib",
             "calonhantu", "tolakcalonhantu", "genggilakuasa", "pedulirakyat",
             "tolakcalonhantudap", "kuasasatuundi", "invokemalaysia",
             "selamatkanundianda", "protectyourvote",  # "tangkapmo1",
             "projekbuangnajib", "see translation", "see original", "daptidakpatriotik",
             "rate translation", "freeanwar", "freepeople", "bebasanwar",
             "bebasrakyat", "anwarmypm", "undurnajib", "anwarbebas",
             "bebasadam", "tangkapnajib", "lawantetaplawan", "onlinemarketing",
             "retweeted", "kamigengnajibrazak", "kamigengnajib", "syukurmalaysiamasihaman",
             "inilahpeluangku", "malaysiakini"]

word_list = [ "membangunbangsa", "memakmurnegeri",
             "calonhantu", "tolakcalonhantu", "genggilakuasa", "pedulirakyat",
             "tolakcalonhantudap", "kuasasatuundi",
             "selamatkanundianda", "protectyourvote",  # "tangkapmo1",
             "see translation", "see original", "daptidakpatriotik",
             "rate translation", "freeanwar", "freepeople",
             "bebasrakyat",
             "bebasadam", "lawantetaplawan", "onlinemarketing",
             "retweeted", "syukurmalaysiamasihaman",
             "inilahpeluangku", "malaysiakini"]

word_replace_list = ["continue reading", "read article", "see more", "click click", "video link",
                     "terkini", "subscribe", "macamni", "dato seri", "last year", "next year",
                     "semalam", "rate video", "video comment",
                     "video embed", "video subscribe", "favorite video",
                     "datuk seri", "dato sri", "baca selanjutnya",
                     "video kinitv"]

month_list = ["januari", "january", "februari", "march", "april", "mei",
              "june", "julai", "july", "ogos", "august", "september", "oktober", "october",
              "november", "december", "disember",
              "sept", "oct", "nov", ]

dayname_list = ["isnin", "monday", "selasa", "tuesday", "rabu",
                "wednesday", "thursday", "jumaat", "friday",
                "sabtu", "saturday", "sunday", "ahad"]

first_word_match_list = ["admin"]


# ----------------------------------
# vectorise sentences & corresponding labels
def vectorize_datalabels(filename):
    data = list()
    labels = list()
    for line in codecs.open(filename, 'r',encoding='utf8').readlines():
        if 1 > len(line.strip()):
            continue
        t = line.split(u"\t")
        data.append(t[0])
        data = [s.strip() for s in data]
        sub_labels = t[1].split(",")
        sub_labels = [s.strip() for s in sub_labels]

        if sub_labels and len(sub_labels) > 0:
            # log.info(len(sub_labels))
            labels.append(sub_labels)
        else:
            raise ValueError("no label")
    return data, labels


# ----------------------------------
# def get content from post
def get_content(log, res_item):

    if "message" in res_item["_source"]["facebook"]["content"]:
        if "text" in res_item["_source"]["facebook"]["content"]["message"]:
            msg_txt = res_item["_source"]["facebook"]["content"]["message"]["text"]
        else:
            msg_txt = ""
        if "shared_content" in res_item["_source"]["facebook"]["content"]["message"]:
            if "shared_text" in res_item["_source"]["facebook"]["content"]["message"]["shared_content"]:
                msg_txt_shared = res_item["_source"]["facebook"]["content"]["message"]["shared_content"][
                    "shared_text"]["text"]
            else:
                msg_txt_shared = ""
        else:
            msg_txt_shared = ""
    else:
        msg_txt = ""
        msg_txt_shared = ""
    # log.info('-----------msg_txt START HERE--------------')
    # log.info ('msg_txt: %s'%(msg_txt))
    # log.info('-----------msg_txt END HERE--------------')
    # log.info('-----------msg_txt_shared START HERE--------------')
    # log.info ('msg_txt_shared: %s'%(msg_txt_shared))
    # log.info('-----------msg_txt_shared END HERE--------------')

    # 2017-07-12 to cater updated profile album
    try:
        story = res_item["_source"]["facebook"]["story"]
    except KeyError:
        story = ""
    #log.info('-----------STORY START HERE--------------')
    #log.info ('story: %s'%(story))
    #log.info('-----------STORY END HERE--------------')



    if msg_txt == "" and msg_txt_shared == "":
        if story and story != "":
            content = story
        else:
            content = ""
    else:
        content = " " + msg_txt + " " + msg_txt_shared
    #log.info('-----------CONTENT START HERE--------------')
    #log.info(content)
    #log.info('-----------CONTENT ENDS HERE--------------')
    return content


# main process and clean text in equence
def process_text(log, mytext, stop_words_in=None, stop_words_en=None):
    # lower case
    mytext = item_lowercase(mytext.split())

    # remove links
    mytext = remove_links(mytext)
    # remove date 12/23/2016 12:34:56
    mytext = remove_dates(mytext)
    # remove money
    mytext = remove_rm(mytext)
    # remove phone numbers
    mytext = remove_phone_no(mytext)
    # remove_emails
    mytext = remove_emails(mytext)

    # cater x.x
    mytext = mytext.replace(".", ". ")
    mytext = mytext.split()

    # remove non-latin
    mytext = remove_non_latin(mytext)
    # remove puncs via regex. non-ascii support
    mytext = remove_puncs_regex(mytext)
    mytext = items_join(mytext)
    mytext = mytext.split()

    # extra stopwords removal
    mytext = remove_words(word_list, mytext)
    mytext = mytext.split()

    # extra remove months
    mytext = remove_words(month_list, mytext)
    mytext = mytext.split()

    # extra remove day names
    mytext = remove_words(dayname_list, mytext)
    mytext = mytext.split()

    # stopwords in
    if stop_words_in == None:
        log.info('Warning. Running without stop word indonesia')
    else:
        mytext = remove_stopwords(mytext, stop_words_in)
        pass

    # stopwords en
    if stop_words_en == None:
        log.info('Warning. Running without stop word english')
    else:
        mytext = remove_stopwords(mytext, stop_words_en)
        pass


    # join list items
    mytext = items_join(mytext)
    # remove singular character s as in najib's
    mytext = remove_singular_char(mytext)

    # remove anymore years
    mytext = remove_numbers(mytext)

    # remove first word if match
    mytext = remove_first_word_match(mytext)

    # remove continue reading & others
    mytext = word_replace(mytext)

    # remove whitespaces
    mytext = util_gen.remove_multiple_whitespace(mytext.split())

    return mytext


# 2017-06-20
def word_replace(mytext):
    # print mytext
    # exit()
    for item in word_replace_list:
        # print item
        mytext = mytext.replace(item, " ")
        # print mytext
    return mytext


# cater human error. sometimes tersalah label ECONOMY EKONOMI
def transform_brand_label(perception):
    # change perception
    perception = perception.upper().split()
    perception = [w.replace('ECONOMY', 'ECONOMIC') for w in perception]
    perception = [w.replace('EKONOMI', 'ECONOMIC') for w in perception]
    perception = [w.replace('RELIGION', 'RELIGIOUS') for w in perception]
    perception = items_join(perception)
    return perception


# remove singular haracters from text. najib's -> najib
def remove_singular_char(mytext):
    new = []
    for i in mytext.split():
        if i != "s" and len(i) > 1:
            new.append(i)
    mytext = items_join(new)
    return mytext


# remove custom word list from sentence
def remove_words(my_word_list, content):
    content = items_join(content)
    for x in my_word_list:
        content = content.replace(x, " ")
    content = content.split()
    content = items_join(content)
    return content


# to lowercase
def item_lowercase(mytext):
    # lower
    mytext = [x.lower() for x in mytext]
    mytext = items_join(mytext)
    return mytext


# remove url
def remove_links(mytext):
    # caters short links
    mytext = re.sub(r"http\S+", "", mytext)
    urlintext = re.compile(
        r'(?:(?:http|https):\/\/)?([-a-zA-Z0-9.]{2,256}\.[a-z]{2,4})\b(?:\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?')
    mytext = urlintext.sub("", mytext)
    return mytext


# remove non-latin
def remove_non_latin(mytext):
    new = []
    for x in mytext:
        # n = regex.sub(ur'[^\p{Latin}]', u'', x) # very good, but it removes br1m pr1m for some reason
        n = re.sub(ur'[^\x00-\x7F\x80-\xFF\u0100-\u017F\u0180-\u024F\u1E00-\u1EFF]', u'', x)
        new.append(n)
    new[:] = [item for item in new if item != '']
    return new


# remove stopwords
def remove_stopwords(content, stop_words):
    content = [w for w in content if w not in stop_words]
    return content


# remove first word if in match_list; eg admin at beginning of sentence
def remove_first_word_match(content):
    content = content.split()
    try:
        if content[0] in first_word_match_list:
            content[0] = ""
    except Exception, ex:
        pass
    content = items_join(content)
    return content


# remove dates
def remove_dates(mytext):
    # mytext = " ".join(mytext)
    mytext = re.sub(r'(\dhb\b)|(\da.m\b)|(\dp.m\b)|'
                    r'(\dam\b)|(\dptg\b)|(\dpg\b)|'
                    r'(\dkm\b)|(\dkg\b)|(\dth\b)'
                    r'|(\dmlm\b)|(\dpagi\b)|(\dpm\b)', '', mytext)
    return mytext


# remove money
def remove_rm(mytext):
    mytext = mytext.split()
    mytext = " ".join(mytext)
    # usd,rm,sgd
    mytext = re.sub(
        r'(usd\dbn)|(\dbn\b)|(\b\dm\b)|(\dh\b)|(\d*cents\b)|(\dsen\b)|(\brm\d*mil\b)|(\brm\d*k)|(\brm\d*\b)|(\busd\d)|(\bsgd\d)',
        '', mytext)
    return mytext


# remove non-ascii puncs
# http://stackoverflow.com/questions/265960/best-way-to-strip-punctuation-from-a-string-in-python
# https://en.wikipedia.org/wiki/Unicode_character_property
# https://pypi.python.org/pypi/regex/2017.04.29#downloads
def remove_puncs_regex(mytext):
    new = []
    remove = regex.compile(ur'[\p{C}|\p{M}|\p{P}|\p{S}|\p{Z}]+', regex.UNICODE)
    for x in mytext:
        s = u"%s" % x
        s = remove.sub(u" ", s).strip()
        new.append(s)
    # remove empty items
    new[:] = [item for item in new if item != '']
    return new


# remove emails
def remove_emails(mytext):
    mytext = re.sub(r'[\w\.-]+@[\w\.-]+', '', mytext)
    return mytext


# remove puncs with string library
def remove_puncs_string(mytext):
    # remove punctuations for every word
    mytext = [x.translate(string.maketrans("", ""), string.punctuation) for x in mytext]
    return mytext


# remove multiple spaces
def items_join(mytext):
    mytext = " ".join(mytext)
    return mytext


# remove phone numbers 011-39376684.
def remove_phone_no(mytext):
    mytext = re.sub(r'\d\d\d-\d\d\d\d\d\d\d', '', mytext)
    return mytext


# remove year, eg 2016
def remove_numbers(mytext):
    # mytext = re.sub(r'\d\d\d\d|\d\d\d', '', mytext)
    # mytext = re.sub(r'\d\d\d', '', mytext)
    # mytext = re.sub(r'\d\d', '', mytext)
    mytext = " ".join([x for x in mytext.split(" ") if not x.isdigit()])
    return mytext
