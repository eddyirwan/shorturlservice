-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 139.59.121.44
-- Generation Time: Oct 29, 2017 at 01:41 PM
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ninetex`
--

-- --------------------------------------------------------

--
-- Table structure for table `urlshorterner`
--

CREATE TABLE `urlshorterner` (
  `id` int(11) NOT NULL,
  `long_url` varchar(500) NOT NULL,
  `short_url` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urlshorterner`
--

INSERT INTO `urlshorterner` (`id`, `long_url`, `short_url`) VALUES
(1, 'http://www.utusan.com.my', '13121X'),
(2, 'http://www.maybank2u.com.my', '12131X'),
(5, 'http://www.facebook.com', '8f462856'),
(6, 'http://www.facebook.com', 'fd333ed2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `urlshorterner`
--
ALTER TABLE `urlshorterner`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `short_url` (`short_url`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `urlshorterner`
--
ALTER TABLE `urlshorterner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
