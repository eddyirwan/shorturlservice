# README #

### What is this repository for? ###

Sample service of shorturl (google short url or tiny url service)

### How do I get set up? ###

1.Using mysql database. only for storing purposes.  
2.Install this software with python enabled.  
 a.  python sakithati.py leader insert 'http://www.facebook.com' (to add this url and it will create a tiny url number)  
 b.  python sakithati.py leader (to create a pickle file from mysql)  
 c.  python sakithati.py httpserver ( to run python http service and bind port 9091)  
3.Dependencies  
 a.  python pip  
 b.  MysqlDB python  
4.Database Configuration  
 a. just follow settings in settings.py in /apps/leader/settings.py  
5.After run sakithati.py httpserver, do mind to test by putting this url at the browser  
 a. http://xxx.xxx.xxx.xxx:9091/12131X  (success)  
 b. http://xxx.xxx.xxx.xxx:9091/eddy12 (404 page)  

### Who do I talk to? ###

Eddy Irwan (eddyirwan@gmail.com)