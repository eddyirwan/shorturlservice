import lib.mvc as mvc
import os,pickle,sys,uuid
import mysql


#sakithati.py leader insert 'http://www.facebook.com'
#sakithati.py leader

class mvc_leader (mvc.mvc_base):
    orm_mysql=None

    def startup(self):
        self.log.info('startup function')
        configuration={
            'PARAMETER':self.PARAMETER,
            'log':self.log,
        }
        self.orm_mysql=mysql.mysql(**configuration)
        self.orm_mysql.connect()

    def run(self):
        #https://www.saltycrane.com/blog/2008/01/saving-python-dict-to-file-using-pickle/
        self.log.info('run function')

        total_argument=len(sys.argv)
        if total_argument == 4:
            self.orm_mysql.put_urlshorterner(sys.argv[3],str(uuid.uuid4())[:8])

        elif total_argument == 2:
            pickle_object=self.orm_mysql.get_urlshorterner()
            output = open(self.PARAMETER.PICKLE_FILE, 'wb')
            pickle.dump(pickle_object, output)
            output.close()
            pkl_file = open(self.PARAMETER.PICKLE_FILE, 'rb')
            mydict2 = pickle.load(pkl_file)
            pkl_file.close()
            self.log.info('Total: %s'%len(mydict2))
            for key,val in mydict2.items():
                self.log.info('Keys: %s \t\t Value:%s'%(key.rjust(20, ' '),val))

        else: 
            self.log.info('abnormal argument')
        self.inject(**{'break_yeild':True})
