import MySQLdb
from lib.generic_class import generic_class

class mysql(generic_class):
    con = None
    cursor = None

    def connect(self):
        self.log.info(self.PARAMETER.HOST)
        db = self.PARAMETER.DATABASE
        self.con = MySQLdb.connect(host=self.PARAMETER.HOST, port=self.PARAMETER.PORT, user=self.PARAMETER.USERNAME,
                              passwd=self.PARAMETER.PASSWORD,
                              db=db)
        self.log.info(self.PARAMETER.HOST)
        self.con.set_character_set('utf8')
        self.cursor = self.con.cursor()
        self.cursor.execute('SET NAMES utf8;')
        self.cursor.execute('SET CHARACTER SET utf8;')
        self.cursor.execute('SET character_set_connection=utf8;')
        return self

    def put_urlshorterner(self,long_url,short_url):
        strsql = "insert into `urlshorterner` (`long_url`,`short_url`) values ('%s','%s')"%(long_url,short_url)
        try:
            self.cursor.execute(strsql)
            self.con.commit()
        except Exception:
            self.log.exception('Error while insert')
        else:
            self.log.info('done saving data')

    def get_urlshorterner(self):

        strsql = "SELECT * FROM urlshorterner"

        self.cursor.execute(strsql)
        results = self.cursor.fetchall()
        dictionary={}
        for x in results:
        	dictionary[x[2]]=x[1]
        self.log.info(dictionary)
        return dictionary
