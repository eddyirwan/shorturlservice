import lib.mvc as mvc
import os,sys
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import re
import pickle



class CustomHandler(BaseHTTPRequestHandler):


    def do_GET(self):
        
        request=self.path.strip("/")
        print request
        if request in self.pickle_file_list:
            try:
                hash_key=self.pickle_file_dict[request]
            except KeyError:
                self.send_error(500)
            else:
                self.send_response(301)
                self.send_header('Location',hash_key)
                self.end_headers()
        else:
            self.send_error(404)
        return


class Server(HTTPServer):

    def inject(self,variable1,variable2):
        self.RequestHandlerClass.pickle_file_dict = variable1
        self.RequestHandlerClass.pickle_file_list = variable2

class mvc_httpserver (mvc.mvc_base):

    def startup(self):
        self.log.info('startup function')
        pkl_file = open(self.PARAMETER.PICKLE_FILE, 'rb')
        self.pickle_object_dict = pickle.load(pkl_file)
        self.pickle_object_list=self.pickle_object_dict.keys()
        pkl_file.close()
        self.log.info('Total: %s'%len(self.pickle_object_dict))

    def run(self):
        PORT = 9091
        server_address = ('', PORT)
        #httpd = SocketServer.ThreadingTCPServer(('', PORT),CustomHandler,123)
        httpd = Server(server_address, CustomHandler)
        httpd.inject(self.pickle_object_dict,self.pickle_object_list)
        httpd.serve_forever()
        self.inject(**{'break_yeild':True})

#http://139.59.121.44:9090/eddy
